<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    Category::create(
        [
            'name' => 'Vinyls',
            'slug' => 'tours',
            

        ]
);

        Category::create(
        [
                'name' => 'CD',
                'slug' => 'cds',
    
    
        ]
);

        Category::create(
            [
                'name' => 'Cassettes',
                'slug' => 'audio_tapes',
            
    
            ]
);

        Category::create(
            [
                'name' => 'Livres: Graphzines',
                'slug' => 'graphzines_books',
                
        
            ]
);
    }
}
