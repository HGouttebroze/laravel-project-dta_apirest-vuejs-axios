<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=!!, initial-scale=1.0">
    <title>Avant-garde</title>
</head>
<body>
    
            <h1>AVANT-GARDE</h1>

<p>Dire d'un artiste qu'il représente l'avant-garde, c'est dire qu'il crée l'art 
académique du futur.</p>

<p>En art, quelques artistes avant-gardistes refusent toute affiliation avec leurs 
prédécesseurs et se placent donc en porte à faux en refusant tout art antérieur. 
Le terme est souvent utilisé en art à propos d'artistes qui « seraient » en avance 
sur leur époque.</p>

<p>Selon l'avant-garde, la valeur d'une œuvre se confond avec son caractère inouï, 
en avance sur son temps. Il n'y a pas un modèle éternel du Beau, l'artiste se doit 
de concentrer dans sa production l'essence de la modernité, encore en gestation, 
de rompre avec les conceptions artisanales de l'art, avec le culte de la nature 
et le réalisme de l'art figuratif. Sous une forme moins directement liée à l'idée 
d'une mission historique de l'artiste, l'avant-gardisme renvoie à une conception 
individualiste de la création. Tout peut devenir art, si l'artiste le décide, 
l'artiste étant libéré de tout stéréotype social ou esthétique. « S'il faut en 
finir avec l'art figuratif, s'il faut cesser d'imiter la nature, c'est pour être 
enfin pleinement en mesure d'exprimer la subjectivité », écrit Luc Ferry à propos 
de Vassily Kandinsky. L'avant-garde oscille entre une conception, fonctionnelle ou 
ludique, de l'art comme partie prenante du monde industriel ou post-moderne, et une 
radicalité provocante, choquante, et pas seulement à l'égard du passé. 
Cette ambiguïté est très sensible chez Andy Warhol, ou plutôt les divergences des 
interprètes. Alain Jouffroy dit du Pop art qu'il « est signifiant : il fige le banal 
quotidien qui fuit autour de nous ; usant d'une technique banale, il tend un miroir 
glacé dans lequel se reflète une civilisation de consommation ».</p>

</body>
</html>